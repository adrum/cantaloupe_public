FROM openjdk:21-slim-buster
LABEL org.opencontainers.image.authors="drummerroma@gmail.com"

ENV VERSION 5.0.5
ENV LIBJPEGTURBO 2.1.2

ENV ADMIN_USERNAME admin
ENV ADMIN_SECRET admin
ENV REDIS_HOST localhost
ENV SOURCE FilesystemSource


EXPOSE 80 8182

RUN apt-get update && apt-get install -y --no-install-recommends \
  build-essential curl unzip gettext-base jq libopenjp2-tools \
  x11-apps cmake nasm \
  && rm -rf /var/lib/apt/lists/*

#RUN apt-get install -y libjpeg62-turbo
#RUN apt-get install -y build-essential

ENV CANTALOUPE_PROPERTIES="/etc/cantaloupe.properties"

RUN adduser --system --home /opt/cantaloupe cantaloupe

# Install libjpeg-turbo
RUN curl -L "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/$LIBJPEGTURBO.zip" > /tmp/libjpegturbo.zip \
  && unzip /tmp/libjpegturbo.zip -d /tmp \
  && cd /tmp/libjpeg-turbo-$LIBJPEGTURBO \
  && cmake -G"Unix Makefiles" -DWITH_JAVA=1 \
  && make \
  && mkdir -p /opt/libjpeg-turbo/lib/ \
  && cp libturbojpeg.so /opt/libjpeg-turbo/lib/libturbojpeg.so

# Retrieve Cantaloupe executable
RUN curl -L "https://github.com/cantaloupe-project/cantaloupe/releases/download/v$VERSION/cantaloupe-$VERSION.zip" > /tmp/cantaloupe.zip \
  && mkdir -p /usr/local/ \
  && cd /usr/local \
  && unzip /tmp/cantaloupe.zip -d /usr/local \
  && ln -s cantaloupe-$VERSION cantaloupe \
  && rm /tmp/cantaloupe.zip \
  && cp /usr/local/cantaloupe-$VERSION/deps/Linux-x86-64/lib/* /lib


# modify cantaloupe configuration copy it into the dirctory
#COPY cantaloupe.properties /etc/cantaloupe.properties
RUN touch /etc/cantaloupe.properties

# replace env variables
# endpoint.admin.username = 
# endpoint.admin.secret = 
#RUN sed -i "s/endpoint.admin.username\ =\ /endpoint.admin.username = ${ADMIN_USERNAME}/" /etc/cantaloupe.properties
#RUN sed -i "s/endpoint.admin.secret\ =\ /endpoint.admin.secret\ =\ ${ADMIN_SECRET}/" /etc/cantaloupe.properties

# REDIS
#RUN sed -i "s/RedisCache.host\ =\ localhost/RedisCache.host\ =\ ${REDIS_HOST}/" /etc/cantaloupe.properties
# source
#RUN sed -i "s/source.static\ =\ FilesystemSource/source.static\ =\ ${SOURCE}/" /etc/cantaloupe.properties


RUN mkdir -p /var/log/cantaloupe \
 && mkdir -p /var/cache/cantaloupe \
 && chown -R cantaloupe /var/log/cantaloupe \
 && chown -R cantaloupe /var/cache/cantaloupe \
 && chown cantaloupe /etc/cantaloupe.properties

RUN mkdir -p /var/log/cantaloupe /var/cache/cantaloupe \
  && chown -R cantaloupe /var/log/cantaloupe /var/cache/cantaloupe /opt/cantaloupe

USER cantaloupe
CMD java -Dcantaloupe.config=$CANTALOUPE_PROPERTIES -Xmx2g -jar /usr/local/cantaloupe/cantaloupe-$VERSION.jar
